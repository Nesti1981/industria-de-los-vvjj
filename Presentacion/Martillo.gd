extends KinematicBody2D

var vec = Vector2()

func _ready():
	name = "disparo"
	
func _process(delta):
	
	move_and_slide(vec)
	
	if get_slide_count() > 0:
		var colision = get_slide_collision(0).collider
		
		if colision == null:
			return
			
func set_vector(vector):
	vec=vector.normalized() * 400
	
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
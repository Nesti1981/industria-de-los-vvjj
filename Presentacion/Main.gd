extends Node2D

export (PackedScene) var martillo
var contador = 0
var disparar = false

func _ready():
	pass
	
func _physics_process(delta):
	
	if disparar:
		if Input.is_action_just_pressed("ui_accept"):
			var mart = Vector2()
			mart = $Nestor/Position2D.global_position 
			var tiro = martillo.instance()
			add_child(tiro)
			tiro.set_vector($Nestor/Position2D2.global_position - mart)
			tiro.global_position = mart
			yield(get_tree().create_timer(.7),"timeout")
			tiro.queue_free()
	
func _on_caja_body_entered(body):
	if body.name == "Nestor":
		contador += 1
		$caja/AnimationPlayer.play("estalla")
		$Label1.visible = true
		$Label1/AnimationPlayer.play("saliendo")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "estalla" and contador == 1:
		$caja.queue_free()
	if anim_name == "estalla" and contador == 2:
		$caja2.queue_free()
	if anim_name == "estalla" and contador == 3:
		$caja3.queue_free()
	if anim_name == "explota" and contador == 4:
		$Label6.visible = true
		$Label6/AnimationPlayer.play("cartel")
		$Enemy1.queue_free()
	if anim_name == "explota" and contador == 5:
		$Label7.visible = true
		$Label7/AnimationPlayer.play("cartel2")
		$Enemy2.queue_free()

func _on_caja2_body_entered(body):
	if body.name == "Nestor":
		contador += 1
		$caja2/AnimationPlayer.play("estalla")
		$Label2.visible = true
		$Label2/AnimationPlayer.play("saliendo")

func _on_caja3_body_entered(body):
	if body.name == "Nestor":
		contador += 1
		$caja3/AnimationPlayer.play("estalla")
		$Label3.visible = true
		$Label3/AnimationPlayer.play("saliendo")


func _on_AreaMartillo_body_entered(body):
	if body.name == "Nestor":
		$martillo.queue_free()
		disparar = true

func _on_Area2D_body_entered(body):
	if body.name == "disparo":
		contador += 1
		$Enemy1/AnimationPlayer.play("explota")
		
func _on_2area_body_entered(body):
	if body.name == "disparo":
		contador += 1
		$Enemy2/AnimationPlayer.play("explota")
extends KinematicBody2D

var movimiento = Vector2()
var velocidad = 200
var gravedad = 10
var direccion = 0
var saltando = false
var colision
var andar = true

func _ready():
	name = "Nestor"
	pass
	
func _physics_process(delta):
	while andar:
		movimiento.y += gravedad
		
		direccion = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	
		if direccion == 1:
			movimiento.x = velocidad
			if saltando == false:
				$AnimationDeNestor.play("adelante")
		elif direccion == -1:
			movimiento.x = -velocidad
			if saltando == false:
				$AnimationDeNestor.play("atras")
		else:
			movimiento.x = 0
			$AnimationDeNestor.stop()
		
		if Input.is_action_just_pressed("ui_up"):
			if saltando == false:
				saltando = true
				movimiento.y = -velocidad * 2
			
		move_and_slide(movimiento)
	
	if get_slide_count() > 0:
		colision = get_slide_collision(0).collider
		
		if colision == null:
			return
			
		if colision.is_in_group("piso"):
			saltando = false

func _on_puerta_body_entered(body):
	if body.name == "Nestor":
		andar = false
		self.globlal_position = $"../castle/Position2D".global_position
